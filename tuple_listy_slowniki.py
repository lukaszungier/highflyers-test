# listy - modyfikowalne
lista1 = [5, 3, 7, "s", 2.5, 0]
print(lista1)
print(len(lista1))
print("Trzeci element:", lista1[2])
lista1[2] = -0.55
print("Trzeci element:", lista1[2])
lista1.append("ostatni")
print(lista1)



# tuple - nie możemy zmieniać zawartości, długości - CONSTANTS
tupla1 = (2, 3, "4", 0.5)
print(tupla1)
print(len(tupla1))
print("Trzeci element:", tupla1[2])
# tupla1[2] = 35 - błąd - nie można zmieniać wartośći

# słowniki: klucz (unikalny) - wartość (nie musi być unikalna)
slownik1 = {"f": 100,
            0: "zero",
            1: "zero",
            2: 2.0}
print(len(slownik1))
print(slownik1)
print(slownik1["f"])
print(slownik1[0])

z = [(112, "x", 2.0),
     [{"x": 1, "a": "b"}, 2.0]
     ]
print(z[1][0]["a"])

zmienna = 20 * "a" + 30 * "b"
print(zmienna)
