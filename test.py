x = 5
y = 5.0
z = "5"
print(x)
print(y)
print(z)
print(type(x), type(y), type(z))

a = 5
print(a)
a = "abc"
print(a)

# type hints - sugestia dla IDE

p: int = 5.2
print(p)

# komentarz wielolinijkowy
"""
test
test
test
"""